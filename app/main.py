from fastapi import FastAPI, Response, status, HTTPException
from fastapi.params import Body
from pydantic import BaseModel
from typing import Optional
import psycopg2
from psycopg2.extras import RealDictCursor

app = FastAPI()


class Post(BaseModel):
    title: str
    content: str
    published: bool


try:
    DBconn = psycopg2.connect(host='localhost', dbname='posts',
                              user='postgres', password='', cursor_factory=RealDictCursor)
    cursor = DBconn.cursor()
    print("Database connection was successful")
except Exception as error:
    print("Fail connecting to DB: ", error)

my_posts = [
    {
        "id": 1,
        "title": "Favorite beaches in FL",
        "content": "City beach, Clearwater"
    },
    {
        "id": 2,
        "title": "Jobs in FL",
        "content": "C# and python developer"
    }
]


def find_post(id):
    for post in my_posts:
        if post["id"] == id:
            return post


@app.get("/")
def root():
    return {"message": "Hello World!!!"}


@app.get("/posts")
def get_posts():
    cursor.execute("""SELECT * FROM posts""")
    posts = cursor.fetchall()
    return {"data": posts}


@app.get("/posts/{id}")
def get_post(id: int, response: Response):
    cursor.execute("""SELECT * FROM posts WHERE id = %s""", (str(id)))
    post = cursor.fetchone()
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"post with id: {id} does not exist")
    return {"data": post}


@app.post("/posts", status_code=status.HTTP_201_CREATED)
def post_posts(body: Post):
    cursor.execute(
        """INSERT INTO posts (title, content, published) VALUES (%s, %s, %s) RETURNING * """,
        (body.title, body.content, body.published))
    new_post = cursor.fetchone()
    DBconn.commit()
    return {"data": new_post}


@app.put("/posts/{id}", status_code=status.HTTP_204_NO_CONTENT)
def update_posts(body: Post, id: int):
    cursor.execute(""" UPDATE posts SET title=%s, content=%s, published=%s WHERE id=%s RETURNING *""",
                   (body.title, body.content, body.published, str(id)))
    updated_post = cursor.fetchone()
    DBconn.commit()
    if not updated_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"post with id: {id} does not exist")


@app.delete("/posts/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_posts(id: int):
    cursor.execute(
        """DELETE FROM posts WHERE id = %s returning *""", (str(id)))
    deleted_post = cursor.fetchone()
    DBconn.commit()
    if not deleted_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"post with id: {id} does not exist")
